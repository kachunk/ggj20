﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healable : MonoBehaviour
{

    private Damageable character;
    // Start is called before the first frame update
    void Start()
    {
        character = gameObject.GetComponent<Damageable>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HealHealth(int amount)
    {
        if (!character.alive)
        {
            return;
        }

        if (character.currentHealth <= 0)
        {
            character.currentHealth = 0;
            character.alive = false;
            gameObject.SetActive(false);
        }

        int tmpHealth = character.currentHealth + amount;
        character.currentHealth  = Mathf.Clamp(tmpHealth, 0, character.maxHealth);
        Health health = character.health;
        if (health != null)
        {
            health.UpdateHearts();
        }
    }
}

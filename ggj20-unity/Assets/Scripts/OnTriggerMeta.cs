﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerMeta : Triggerable
{
    public Triggerable[] list;
    public override void trigger()
    {
        foreach (Triggerable t in list) {
            t.trigger();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

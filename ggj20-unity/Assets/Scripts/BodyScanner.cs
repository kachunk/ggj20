﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyScanner : MonoBehaviour
{
    HashSet<BodyAssembler> bodies = new HashSet<BodyAssembler>();
    BodyAssembler highlight = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire2") && highlight != null)
        {
            BodyAssembler ass = BodyPart.getAss(transform);
            GameObject tmp = highlight.Arms.Anchor;
            ArmsBodyPart tmpb = ass.Arms;
            ass.Arms.Release();
            ass.Arms = highlight.Arms;
            highlight.Arms.Anchor = tmpb.Anchor;
            highlight.Arms = tmpb;
            tmpb.Anchor = tmp;
        }
        if (Input.GetButtonDown("Fire3") && highlight != null)
        {
            BodyAssembler ass = BodyPart.getAss(transform);
            GameObject tmp = highlight.Legs.Anchor;
            LegsBodyPart tmpb = ass.Legs;
            ass.Legs = highlight.Legs;
            highlight.Legs.Anchor = tmpb.Anchor;
            highlight.Legs = tmpb;
            tmpb.Anchor = tmp;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BodyPart bp = collision.GetComponent<BodyPart>();
        if(bp != null && bp.ass != null) {
            bodies.Add(bp.ass);
        }
        highlightClosest();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        BodyPart bp = collision.GetComponent<BodyPart>();
        if (bp != null && bp.ass != null)
        {
            bodies.Remove(bp.ass);
        }
        highlightClosest();
    }

    private void highlightClosest()
    {
        BodyAssembler closest = null;
        float dist = float.MaxValue;
        foreach(BodyAssembler ass in bodies)
        {
            float newDist = (ass.transform.position - transform.position).sqrMagnitude;
            if (closest == null || dist > newDist)
            {
                closest = ass;
                dist = newDist;
            }
        }
        if(closest != highlight)
        {
            if(highlight != null)
            {
                highlight.stopHighlight();
            }
            highlight = closest;
            if(highlight != null)
            {
                highlight.startHighlight();
            }
        }
    }
}

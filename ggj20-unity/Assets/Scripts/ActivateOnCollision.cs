﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnCollision : MonoBehaviour
{
    private bool deactivated = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!deactivated)
        {
            BloodCollector collider = gameObject.GetComponentInChildren<BloodCollector>(true);
            if (null == collider)
            {
                return;
            }

            GameObject child = collider.gameObject;
            child.SetActive(true);
            deactivated = true;
        }
    }
}

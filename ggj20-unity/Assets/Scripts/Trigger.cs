﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{

    public Triggerable[] succesTrigger;

    public Triggerable[] errorTrigger;

    public Condition[] conditions;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ExecuteTrigger()
    {
        bool success = true;
        foreach (Condition condition in conditions)
        {
            if (!condition.isFullfilled())
            {
                success = false;
                break;
            }
        }

        if (success)
        {
            foreach (Triggerable trigger in succesTrigger)
            {
                trigger.trigger();
            }
        }
        else
        {
            foreach (Triggerable trigger in errorTrigger)
            {
                trigger.trigger();
            }
        }
    }
}

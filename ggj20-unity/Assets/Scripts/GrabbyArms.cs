﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabbyArms : Use
{
    public BodyPart carryItem = null;
    public GameObject carryAnchor;
    public HashSet<BodyPart> bodies = new HashSet<BodyPart>();
    public BodyPart highlight = null;

    public void Start()
    {
        carryAnchor = BodyAssembler.FindDeepChild(transform.parent, "CarryAnchor").gameObject;
    }
    public override void UseIt()
    {
        if(carryItem != null)
        {
            Release();
            highlightClosest();
        }
        else if(highlight != null)
        {
            highlight.Anchor = carryAnchor;
            carryItem = highlight;
            highlightClosest();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        BodyPart bp = collision.GetComponent<BodyPart>();
        if (bp != null)
        {
            bodies.Add(bp);
        }
        highlightClosest();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        BodyPart bp = collision.GetComponent<BodyPart>();
        if (bp != null)
        {
            bodies.Remove(bp);
        }
        highlightClosest();
    }

    private void highlightClosest()
    {
        BodyPart closest = null;
        float dist = float.MaxValue;
        foreach (BodyPart bp in bodies)
        {
            float newDist = (bp.transform.position - transform.position).sqrMagnitude;
            if (closest == null || dist > newDist)
            {
                closest = bp;
                dist = newDist;
            }
        }
        if (closest != highlight)
        {
            if (highlight != null)
            {
                BodyAssembler.setHighlight(highlight, false);
            }
            highlight = closest;
            if (highlight != null)
            {
                if(carryItem == null)
                {
                    BodyAssembler.setHighlight(highlight, true);
                } else
                {
                    BodyAssembler.setHighlight(highlight, false);
                    highlight = null;
                }
            }
        }
    }

    public override void Release()
    {
        if (carryItem != null) { 
            carryItem.Anchor = null;
            carryItem.Dismember();
            carryItem = null;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class LockOpenTriggerables : Triggerable
{
    public LockOpenCondition lockOpenCondition;

    public override void trigger()
    {
        if (lockOpenCondition != null)
        {
            lockOpenCondition.setFullfilled(true);
        }
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

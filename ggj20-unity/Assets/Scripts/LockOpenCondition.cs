﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOpenCondition : Condition
{

    private bool fullfilled = false;

    public override bool isFullfilled()
    {
        return fullfilled;
    }

    public override void setFullfilled(bool fullfilled)
    {
        this.fullfilled = fullfilled;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

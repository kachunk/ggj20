﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAIcontroll : Movecontrol
{
    float moveDir = -1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    new void Update()
    {
        if (leftWallSensor.isTouching || leftWallSensor.softTarget)
        {
            moveDir = 1f;
        }
        if(rightWallSensor.isTouching || rightWallSensor.softTarget)
        {
            moveDir = -1f;
        }
        horizontal = moveDir;
        base.Update();
    }
}

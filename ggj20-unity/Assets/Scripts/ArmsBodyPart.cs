﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsBodyPart : BodyPart
{
    [SerializeField]
    public Use use;
    virtual public void Use()
    {
        if(use != null)
        {
            use.UseIt();
        }
    }
    public void Release()
    {
        if (use != null)
        {
            use.Release();
        }
    }
    new public void Start()
    {
        base.Start();
    }
}

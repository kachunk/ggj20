﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{

    public int maxHealth = 10;
    public int currentHealth = 0;
    public bool alive = true;
    public Health health;


    // Start is called before the first frame update
    void Start()
    {
        health = gameObject.GetComponent<Health>();
        alive = true;
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

    }

    public void SetMaxHealth(int maxHealth)
    {
        this.maxHealth = maxHealth;
        initHearts();
        updateHearts();
    }

    private void updateHearts()
    {
        if (health != null)
        {
            health.UpdateHearts();
        }
    }

    private void initHearts()
    {
       if (health != null)
        {
            health.CheckHealthAmount();
            health.UpdateHearts();
        }
    }

    public void TakeDamage(int amount)
    {
        if(!alive)
        {
            return;
        }


        int tmpHealth = currentHealth - amount;
        
        currentHealth = Mathf.Clamp(tmpHealth, 0, maxHealth); 
        if (health != null)
        {
            health.UpdateHearts();
        }
        if(currentHealth <= 0)
        {
            currentHealth = 0;
            alive = false;


            Movecontrol movecontrol = gameObject.GetComponent<Movecontrol>();
            if(movecontrol != null)
            {
                movecontrol.Die();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

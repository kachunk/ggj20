﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Use : MonoBehaviour
{
    public abstract void UseIt();
    public abstract void Release();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageStar : MonoBehaviour
{
    SpriteRenderer star;
    public float duration = 0.5f;
    public float speed = 1f;
    public float startTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        star = GetComponent<SpriteRenderer>();
        GetComponent<Rigidbody2D>().velocity = new Vector3(Random.value - 0.5f, Random.value, 0) * speed;
        startTime = Time.timeSinceLevelLoad;
    }

    // Update is called once per frame
    void Update()
    {
        star.color = new Color(1, 1, 1, Mathf.Lerp(1, 0, (Time.timeSinceLevelLoad - startTime) / duration));
        if(Time.timeSinceLevelLoad - startTime > duration)
        {
            Destroy(this);
        }
    }
}

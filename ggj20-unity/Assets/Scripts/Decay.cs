﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decay : MonoBehaviour
{

    public float timeToLife = 0;
    public float decalPercent = 0.3f;
    private float remaining;
    public bool doDecay = true;
    // Start is called before the first frame update
    void Start()
    {
        remaining = timeToLife;
    }

    // Update is called once per frame
    void Update()
    {
        if (doDecay)
        {
            if (remaining > 0f)
            {
                SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                if (null != spriteRenderer)
                {
                    float percent = Mathf.Min((remaining / timeToLife) / decalPercent, 1f);
                    spriteRenderer.color = new Color(1f, 1f, 1f, percent);
                }
                remaining -= Time.deltaTime;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

    }

}

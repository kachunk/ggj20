﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public GameObject pausePanel;
    public GameObject continueButton;
    public GameObject nextLevelButton;

    public Text panelText;
    public AudioSource audioSource;
    public AudioClip pauseMusic;
    public AudioClip gameMusic;

    private bool continueEnabledInMenue = false;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(pausePanel != null)
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            {
                if (!pausePanel.activeInHierarchy)
                {
                    ShowScreen("Pause", false, true);
                } else if (pausePanel.activeInHierarchy && continueEnabledInMenue)
                {
                    ContinueGame();
                }
            }
        }

    }

    public void ChangeScene(int sceneNumber)
    {
        Time.timeScale = 1;
        if(pausePanel != null)
        {
            pausePanel.SetActive(false);
        }
        SceneManager.LoadScene(sceneNumber);
    }

    public void NextLevel()
    {
        int sceneNumber = SceneManager.GetActiveScene().buildIndex + 1;
        Time.timeScale = 1;
        if (pausePanel != null)
        {
            pausePanel.SetActive(false);
        }
        SceneManager.LoadScene(sceneNumber);
    }

    public void RestartLevel() {
        Time.timeScale = 1;
        if (pausePanel != null)
        {
            pausePanel.SetActive(false);
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ShowScreen(string title, bool enableNextLevel, bool enableContinue)
    {
        continueEnabledInMenue = enableContinue;
        Time.timeScale = 0;
        audioSource.Stop();
        audioSource.clip = pauseMusic;
        audioSource.Play();
        panelText.text = title;
        pausePanel.SetActive(true);
        nextLevelButton.SetActive(enableNextLevel);
        continueButton.SetActive(enableContinue);
    }

    public void ContinueGame()
    {
        audioSource.Stop();
        audioSource.clip = gameMusic;
        audioSource.Play();
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void StopGame()
    {
        Debug.Log("Stoping Game");
        Application.Quit();
    }
}

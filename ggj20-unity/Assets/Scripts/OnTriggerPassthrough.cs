﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerPassthrough : Triggerable
{
    private bool triggered = false;
    public override void trigger()
    {
        if (!triggered)
        {
            foreach (BoxCollider2D c in gameObject.GetComponents<BoxCollider2D>())
            {
                c.enabled = false;
            }
            triggered = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

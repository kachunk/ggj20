﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class BloodSpawner : Triggerable
{
    public List<GameObject> possibleSpawns = new List<GameObject>();
    public List<GameObject> toSpawn = new List<GameObject>();

    public int count = 0;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void Spawn()
    {
        //Position
        Transform transform = gameObject.transform;
        //enable
        for (int i = 0; i < count; ++i)
        {
            GameObject spawn = GetRandomSpawn();
            GameObject spawned = Instantiate(spawn, transform.position, transform.rotation);
            Rigidbody2D body = spawned.GetComponent<Rigidbody2D>();
            body.velocity = GetRandomVelocity();
            spawned.transform.Rotate(0f, 0f, Random.Range(0, 360));

        }
      
    }

    // Update is called once per frame
    void Update()
    {

    }

    private GameObject GetRandomSpawn()
    {
        return possibleSpawns[Random.Range(0, possibleSpawns.Count)];
    }

    public override void trigger()
    {
        Spawn();
    }

    private Vector2 GetRandomVelocity() {
        return new Vector2(Random.Range(-4f, 4f), 9f);
    }
}

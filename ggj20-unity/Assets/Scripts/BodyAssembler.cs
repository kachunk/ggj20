﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyAssembler : MonoBehaviour
{
    private Movecontrol mc;
    [SerializeField]
    private BodyPart body;
    public BodyPart Body {
        set 
        {
            body = value;
        }
        get
        {
            return body;
        }
    }
    [SerializeField]
    private LegsBodyPart legs;
    public LegsBodyPart Legs {
        set
        {
            legs = value;
            mc.legs = value;
        }
        get
        {
            return legs;
        }
    }
    [SerializeField]
    private ArmsBodyPart arms;
    public ArmsBodyPart Arms
    {
        set
        {
            arms = value;
        }
        get
        {
            return arms;
        }
    }
    [SerializeField]
    private BodyPart head;
    public BodyPart Head
    {
        set
        {
            head = value;
        }
        get
        {
            return head;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        legs = FindDeepChild(transform, "LegAnchor").GetChild(0).GetComponent<LegsBodyPart>();
        arms = FindDeepChild(transform, "ArmAnchor").GetChild(0).GetComponent<ArmsBodyPart>();
        body = FindDeepChild(transform, "BodyAnchor").GetChild(0).GetComponent<BodyPart>();
        head = FindDeepChild(transform, "HeadAnchor").GetChild(0).GetComponent<BodyPart>();
        mc = GetComponent<Movecontrol>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public static Transform FindDeepChild(Transform aParent, string aName)
    {
        Queue<Transform> queue = new Queue<Transform>();
        queue.Enqueue(aParent);
        while (queue.Count > 0)
        {
            var c = queue.Dequeue();
            if (c.name == aName)
                return c;
            foreach (Transform t in c)
                queue.Enqueue(t);
        }
        return null;
    }

    public void Explode()
    {
        body.Dismember();
        legs.Dismember();
        arms.Dismember();
        head.Dismember();
    }

    public void startHighlight()
    {
        setHighlight(body, true);
        setHighlight(arms, true);
        setHighlight(legs, true);
    }

    public static void setHighlight(BodyPart part, bool enabled)
    {
        foreach(SpriteRenderer r in part.GetComponentsInChildren<SpriteRenderer>())
        {
            r.color = enabled ? Color.yellow : Color.white;
        }
    }

    public void stopHighlight()
    {
        setHighlight(body, false);
        setHighlight(arms, false);
        setHighlight(legs, false);
    }

}

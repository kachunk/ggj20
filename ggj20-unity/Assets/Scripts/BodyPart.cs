﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    public BodyAssembler ass;
    private Vector3 startPos = Vector3.zero;
    private Quaternion startRotation = Quaternion.identity;
    private Vector3 startLocalScale = Vector3.zero;
    public float height = 1;
    public float snapDuration = .2f;
    public float dismemberForce = 3f;
    private float progress = 99999f;
    public float heightOffset = 0f;
    private GameObject anchor;
    public GameObject Anchor
    {
        set
        {
            Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
            if(rb != null)
            {
                rb.simulated = false;
            }
            if(value == null)
            {
                transform.parent = null;
            } else
            {
                transform.parent = value.transform;
            }
            startPos = transform.position;
            startRotation = transform.rotation;
            startLocalScale = transform.localScale;
            progress = 0f;
            anchor = value;
            ass = getAss(transform);
            if(anchor != null)
            {
                StartCoroutine(moveToTarget());
            }
        }
        get
        {
            return anchor;
        }
    }
    // Start is called before the first frame update
    public void Start()
    {
        Transform par = transform.parent;
        if(par != null)
        {
            anchor = par.gameObject;
        }
        ass = getAss(transform);
    }

    public static BodyAssembler getAss(Transform transf)
    {
        BodyAssembler res = null;
        Transform trans = transf;
        while(res == null && trans != null)
        {
            res = trans.gameObject.GetComponent<BodyAssembler>();
            trans = trans.parent;
        }
        return res;
    }

    public void Dismember()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
        if(rb != null)
        {
            rb.simulated = true;
            rb.velocity = new Vector2(Random.value - 0.5f, Random.value) * dismemberForce;
        }
    }

    IEnumerator moveToTarget()
    {
        while(progress < snapDuration)
        {
            progress += Time.fixedDeltaTime;
            transform.position = Vector3.Lerp(startPos, anchor.transform.position, progress / snapDuration);
            transform.rotation = Quaternion.Slerp(startRotation, anchor.transform.rotation,  progress / snapDuration);
            transform.localScale = Vector3.Lerp(startLocalScale, anchor.transform.localScale, progress / snapDuration);
            yield return new WaitForFixedUpdate();
        }
        UpdateHitBox(anchor);
        ResetColor(anchor);
    }

    private void ResetColor(GameObject gameObject)
    {
        BodyPart[] parts = gameObject.GetComponentsInChildren<BodyPart>();
        foreach (BodyPart part in parts)
        {
            BodyAssembler.setHighlight(part, false);
        }
    }

    private void UpdateHitBox(GameObject gameObject)
    {
        GameObject root = FindBoxCollider2dParent(gameObject);
        if (root == null)
        {
            return;
        }
        Bounds bounds = CalculateBounds(BodyAssembler.FindDeepChild(root.transform, "BodyAnchor").gameObject);
        BoxCollider2D collider = root.GetComponent<BoxCollider2D>();
        BodyPart[] parts = root.GetComponentsInChildren<BodyPart>();
        float offset = 0f;
        foreach (BodyPart part in parts)
        {
            offset += part.heightOffset;
        }
        // Debug.Log(root.name + ": actual height: " + collider.size.y + ",\tnew height: " + (bounds.size.y) + ",\tactual offset: " + collider.offset + ",\tnew center: " + bounds.center);
        // Debug.Log("offset: " + offset);
        collider.size = new Vector2(collider.size.x, bounds.size.y + offset);
    }

    private GameObject FindBoxCollider2dParent(GameObject gameObject)
    {
        Transform t = gameObject.transform;
        while (t.parent != null)
        {
            if (t.parent.gameObject.GetComponent<BoxCollider2D>())
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null;
    }

    private Bounds CalculateBounds(GameObject gameObject)
    {
        SpriteRenderer[] srends = gameObject.GetComponentsInChildren<SpriteRenderer>();
        Bounds bounds = new Bounds(gameObject.transform.position, Vector3.zero);
        foreach (SpriteRenderer srend in srends)
        {
            bounds.Encapsulate(srend.bounds);
            // bounds.Encapsulate(srend.sprite.bounds);
        }
        return bounds;
    }
}

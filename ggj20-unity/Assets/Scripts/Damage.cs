﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    public int damage = 1;


    public bool terrain = false;

    private bool dmg_restore;
    public float tickTime = 2;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void DamageStars(int damage, Vector3 position)
    {
        for (int i = 0; i < damage; ++i) {
            Instantiate(Resources.Load("DamageStar"), position, Quaternion.identity);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Damageable damageable = collision.gameObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            if (isDamageable(damageable))
            {
                if (!checkSameHierachy(damageable))
                {
                    damageable.TakeDamage(damage);
                    DamageStars(damage, collision.ClosestPoint(damageable.transform.position));
                    if (terrain && !dmg_restore)
                    {
                        dmg_restore = true;
                        Invoke("DmgRestore", tickTime);
                    }
                }
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        Damageable damageable = collision.gameObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            if (isDamageable(damageable))
            {

                if (!checkSameHierachy(damageable))
                {
                    if (terrain && !dmg_restore)
                    {
                        damageable.TakeDamage(damage);
                        DamageStars(damage, collision.contacts[0].point);
                        dmg_restore = true;
                        Invoke("DmgRestore", tickTime);
                    }
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Damageable damageable = collision.gameObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            if (isDamageable(damageable))
            {

                if (!checkSameHierachy(damageable))
                {
                    if (terrain && !dmg_restore)
                    {
                        damageable.TakeDamage(damage);
                        DamageStars(damage, collision.ClosestPoint(damageable.transform.position));
                        dmg_restore = true;
                        Invoke("DmgRestore", tickTime);
                    }
                }
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Damageable damageable = collision.gameObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            if (isDamageable(damageable))
            {

                if (!checkSameHierachy(damageable))
                {
                    damageable.TakeDamage(damage);
                    DamageStars(damage, collision.contacts[0].point);
                    if (terrain && !dmg_restore)
                    {
                        dmg_restore = true;
                        Invoke("DmgRestore", tickTime);
                    }
                }
            }
        }
    }

    private bool checkSameHierachy(Damageable otherDamageable)
    {
        Damageable damageable = gameObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            if (damageable.Equals(otherDamageable))
            {
                return true;
            }
        }
        Transform t = gameObject.transform;
        while (t.parent != null)
        {
            damageable = t.parent.GetComponent<Damageable>();
            if (damageable != null)
            {
                if (damageable.Equals(otherDamageable))
                {
                    return true;
                }
            }
            t = t.parent.transform;
        }
        return false;
    }

    private void DmgRestore()
    {
        dmg_restore = false;
    }

    private bool isDamageable(Damageable otherDamageable)
    {
        if (!terrain)
        {
            return true;
        }
        LegsBodyPart legsBodyPart = otherDamageable.GetComponentInChildren<LegsBodyPart>();
        if (legsBodyPart != null)
        {
            return !legsBodyPart.canMoveOverDeadlyTerrain;
        }
        return true;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHeart : Use
{
    public override void Release()
    {
    }

    public override void UseIt()
    {
        Instantiate(Resources.Load("Heart"), transform.position, Quaternion.identity);
    }
}

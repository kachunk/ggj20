﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCollisionCondition<T> : Condition

{
    public T type;

    private bool fullfilled = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        
        if(collType != null)
        {
            setFullfilled(true);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }


    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        if (collType != null)
        {
            setFullfilled(true);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        if (collType != null)
        {
            setFullfilled(false);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        if (collType != null)
        {
            setFullfilled(true);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        if (collType != null)
        {
            setFullfilled(true);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        T collType = collision.gameObject.GetComponent<T>();
        if (collType != null)
        {
            setFullfilled(false);
        }
        else
        {
            collType = collision.gameObject.GetComponentInChildren<T>();
        }
    }

    public override bool isFullfilled()
    {
        return fullfilled;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void setFullfilled(bool fullfilled)
    {
        this.fullfilled = fullfilled;
    }
}

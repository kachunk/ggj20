﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegsBodyPart : BodyPart
{
    public bool canClimb = true;
    public bool canMoveOverDeadlyTerrain = false;
    public bool canWalljump = true;
    public float moveSpeed = 5f;
    public float jumpSpeed = 3f;
    public float hopSpeed = 2f;
    public float climbSpeed = 1f;
    public Movecontrol.MovementMode moveMode = Movecontrol.MovementMode.HOP;
}

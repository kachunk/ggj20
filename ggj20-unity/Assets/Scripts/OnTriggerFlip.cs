﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerFlip : Triggerable
{
    public enum FlipDirection {
        Left,
        Right
    }
    private bool triggered = false;
    public FlipDirection direction;
     
    public override void trigger()
    {
        if (!triggered)  {
            Vector3 xScale = gameObject.transform.localScale;
            xScale.x *= -1;
            gameObject.transform.localScale = xScale;

            float xPos = gameObject.transform.position.x;
            SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            float x = spriteRenderer.size.x;

            if (FlipDirection.Left == direction) {
                x *= -1;
            }

            Vector3 position = gameObject.transform.position;
            position.x += x;
            gameObject.transform.position = position;

            triggered = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverTriggerable : Triggerable
{
    public override void trigger()
    {
        GameController gameController = gameObject.GetComponent<GameController>();
        if(gameController != null)
        {
            gameController.ShowScreen("Game Over", false, false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

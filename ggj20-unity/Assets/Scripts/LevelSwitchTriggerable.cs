﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSwitchTriggerable : Triggerable
{
    public override void trigger()
    {
        GameController gameController = gameObject.GetComponent<GameController>();
        if(gameController != null) {
            gameController.ShowScreen("You're Winner !",true,false);
                }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

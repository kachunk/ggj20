﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour
{

    public int heal;

    private Healable healable;

    private float targetDistance = 1.1f;

    private int speed = 8;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(healable != null)
        {
            Vector3 position = healable.gameObject.transform.position;
            Vector3 position1 = transform.parent.gameObject.transform.position;
            Vector3 result = (position - position1);
            if (result.magnitude < targetDistance)
            {
                healable.HealHealth(heal);
                transform.parent.gameObject.SetActive(false);
                gameObject.SetActive(false);

            }
            else
            {
                Vector2 vektor = result.normalized * speed;
                transform.parent.gameObject.GetComponent<Rigidbody2D>().velocity = vektor;
            }
        }
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        Healable healable = collision.gameObject.GetComponent<Healable>();
        if(healable != null)
        {
            Decay decay = gameObject.GetComponentInChildren<Decay>();
            if(decay != null)
            {
                decay.doDecay = false;
            }
            this.healable = healable;
        }
    }
}

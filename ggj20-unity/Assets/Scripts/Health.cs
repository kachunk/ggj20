﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    private int maxHeartAmount;
    private int healthPerHeart = 2;
    public Image[] health;
    public Sprite[] healthSprites;
    public Sprite emptyHeart;
    public Sprite fullHeart;
    public Sprite halfHeart;

    public Damageable character; 


    // Start is called before the first frame update
    void Start()
    {
        SingletonUI instance = SingletonUI.GetSingletonUI();
        Heart[] hearts = instance.gameObject.GetComponentsInChildren<Heart>();
        health = new Image[hearts.Length];
        foreach(Heart heart in hearts)
        {
            Image image = heart.gameObject.GetComponent<Image>();
            health[heart.index-1] = image;
        }

        healthSprites = new Sprite[3];
        healthSprites[0] = emptyHeart;
        healthSprites[1] = halfHeart;
        healthSprites[2] = fullHeart;
        maxHeartAmount = health.Length;
        character = gameObject.GetComponent<Damageable>();
        CheckHealthAmount();
        UpdateHearts();

    }

    public void CheckHealthAmount()
    {
        for(int i = 0; i< maxHeartAmount; i++)
        {
            if(character.maxHealth / 2 <= i)
            {
                health[i].enabled = false;
            }
            else
            {
                health[i].enabled = true;
            }
        }
    }

    public void UpdateHearts()
    {

        if (!character.alive)
        {
            return;
        }
        bool empty = false;
        int i = 0;
        foreach (Image image in health)
        {
            if(empty)
            {
                image.sprite = healthSprites[0];
            }
            else
            {
                i++;
                if(character.currentHealth >= i * healthPerHeart)
                {
                    image.sprite = healthSprites[healthSprites.Length -1];
                }
                else
                {
                    int currentHeartHealth = (int)(healthPerHeart - (healthPerHeart * i - character.currentHealth));
                    int healthPerImage = healthPerHeart / (healthSprites.Length - 1);
                    int imageIndex = currentHeartHealth / healthPerImage;
                    image.sprite = healthSprites[imageIndex];
                    empty = true;
                }
            }
        }
    }
}

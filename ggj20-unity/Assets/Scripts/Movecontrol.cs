﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movecontrol : MonoBehaviour
{
    public Animator animator;
    public BodyAssembler ass;
    public Rigidbody2D body;
    public float horizontal;
    public GroundSensor groundSensor;
    public GroundSensor leftWallSensor;
    public GroundSensor rightWallSensor;
    private BoxCollider2D bounds = null;
    public bool isDead = false;
    public float progress = 999999f;
    public float turnDuration = 0.2f;
    public float targetDirection = 1;
    private float prevDirection = 1;
    public bool triggerLanded = false;
    public bool moveOffScreen = false;
    public LegsBodyPart legs;

    private float defaultSoundVolume = 1.0f;
    private float jumpVolume = 0.5f;
    private float hopVolume = 0.75f;
    public AudioClip walkSound;
    public AudioClip hopSound;
    public AudioClip jumpSound;
    public AudioClip attackSound;
    public AudioClip dieSound;
    public bool isPlayer = false;
    public GameObject gameMenueCanvas;
    public bool hasWalljumped = false;

    private float startPosX;
    private float startPosY;

    void Start()
    {
        startPosX = ass.transform.position.x;
        startPosY = ass.transform.position.y;
    }

    public enum JumpPhase
    {
        NOT_JUMPING,
        HOP,
        JUMP_STARTED,
        JUMPING_UP,
        FALLING_DOWN
    }
    public JumpPhase jumpphase;

    public enum MovementMode
    {
        WALK,
        HOP
    }

    public Dictionary<JumpPhase, float> additionalGravityMultiplier = new Dictionary<JumpPhase, float> {
        { JumpPhase.NOT_JUMPING, 0.0f}, 
        { JumpPhase.HOP, -0.5f }, 
        { JumpPhase.JUMP_STARTED, -0.5f }, 
        { JumpPhase.JUMPING_UP, -0.5f }, 
        { JumpPhase.FALLING_DOWN, 0.0f }
    };

    public void Awake()
    {
        ass = GetComponent<BodyAssembler>();
        body = gameObject.GetComponent<Rigidbody2D>();
        legs = GetComponentInChildren<LegsBodyPart>();
        groundSensor = BodyAssembler.FindDeepChild(transform, "groundSensor").gameObject.GetComponent<GroundSensor>();
        leftWallSensor = transform.Find("leftWallSensor").gameObject.GetComponent<GroundSensor>();
        rightWallSensor = transform.Find("rightWallSensor").gameObject.GetComponent<GroundSensor>();
        try
        {
            bounds = GameObject.FindGameObjectWithTag("LevelBoundries").GetComponentInChildren<BoxCollider2D>();
        } catch (Exception _)
        {
            bounds = null;
        }
        jumpphase = JumpPhase.NOT_JUMPING;
    }

    void FixedUpdate()
    {
        if (isDead)
        {
            return;
        }

        if(!IsInBound())
        {
            if(isPlayer)
            {
                Die();
            } else
            {
                ass.transform.position = new Vector3(startPosX, startPosY, ass.transform.position.z);
            }
        }

        if (!moveOffScreen && !IsVisibleOnScreen())
        {
            return;
        }
        if(legs.moveMode == MovementMode.HOP && Mathf.Abs(horizontal) > 0.1 && jumpphase == JumpPhase.NOT_JUMPING)
        {
            jumpphase = JumpPhase.HOP;
        }
        if((jumpphase == JumpPhase.JUMPING_UP || jumpphase == JumpPhase.FALLING_DOWN) && groundSensor.isTouching && body.velocity.y < 0.1f)
        {
            Touchdown();
            triggerLanded = true;
        }
        float jumpVelocity = 0f;
        if(jumpphase == JumpPhase.JUMP_STARTED)
        {
            PlaySound(jumpSound, jumpVolume);
            jumpphase = JumpPhase.JUMPING_UP;
            jumpVelocity = legs.jumpSpeed;
        }
        if (jumpphase == JumpPhase.HOP)
        {
            PlaySound(hopSound, hopVolume);
            jumpphase = JumpPhase.JUMPING_UP;
            jumpVelocity = legs.hopSpeed;
        }

        float ySpeed = body.velocity.y + jumpVelocity + Physics.gravity.y * additionalGravityMultiplier[jumpphase] * Time.fixedDeltaTime;
        //if(legs.canClimb && ((leftWallSensor.isTouching && horizontal < -0.1f) || (rightWallSensor.isTouching && horizontal > 0.1f)))
        //{
        //    ySpeed = legs.climbSpeed;
        //    horizontal = 0;
        //    jumpphase = JumpPhase.NOT_JUMPING;
        //    triggerLanded = true;
        //}

        if(leftWallSensor.isTouching && jumpphase == JumpPhase.JUMP_STARTED)
        {
            PlaySound(jumpSound, jumpVolume);
            horizontal = 1;
            ySpeed = legs.jumpSpeed;
            jumpphase = JumpPhase.JUMPING_UP;
        }
        else if (rightWallSensor.isTouching && jumpphase == JumpPhase.JUMP_STARTED)
        {
            PlaySound(jumpSound, jumpVolume);
            horizontal = -1;
            ySpeed = legs.jumpSpeed;
            jumpphase = JumpPhase.JUMPING_UP;
        }

        body.velocity = new Vector2(horizontal * legs.moveSpeed, ySpeed);

        if (legs.moveMode == MovementMode.WALK && jumpphase == JumpPhase.NOT_JUMPING && horizontal != 0
            && groundSensor.isTouching)
        {
            PlaySound(walkSound, defaultSoundVolume);
        }

        if (Mathf.Abs(horizontal) > 0.1f && Mathf.Sign(horizontal) != targetDirection)
        {
            targetDirection = Mathf.Sign(horizontal);
            progress = 0;// Mathf.Min(1f - progress / turnDuration, 1f);
            //StopCoroutine("turnAround");
            StartCoroutine(turnAround());
        }
    }

    public void Update()
    {
        UpdateAnimation();
    }

    void UpdateAnimation()
    {
        Animator[] anims = GetComponentsInChildren<Animator>();
        bool isJumping = (jumpphase != JumpPhase.NOT_JUMPING);
        bool isWalking = legs.moveMode != MovementMode.HOP && !isJumping && Mathf.Abs(horizontal) > 0.1;
        foreach (Animator anim in anims)
        {
            anim.SetBool("isWalking", isWalking);
            anim.SetBool("isJumping", isJumping);
            anim.enabled = !isDead;
        }

    }

    public void Touchdown()
    {
        jumpphase = JumpPhase.NOT_JUMPING;
        hasWalljumped = false;
    }

    public void Die()
    {
        isDead = true;
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            rb.simulated = false;
        }
        if(ass != null)
        {
            PlaySound(dieSound, defaultSoundVolume);
            ass.Explode();
        }
        
        BloodSpawner bs = gameObject.GetComponentInChildren<BloodSpawner>();
        if(bs != null)
        {
            bs.trigger();
        }
        UpdateAnimation();
        if(isPlayer)
        {
            if (gameMenueCanvas != null)
            {
                GameController gameController = gameMenueCanvas.GetComponent<GameController>();
                if (gameController != null)
                {
                    gameController.ShowScreen("You Dead", false, false);
                }
            }
        }
    }

    IEnumerator turnAround()
    {
        while (progress < turnDuration)
        {
            progress += Time.deltaTime;
            Vector3 newScale = new Vector3(Mathf.Lerp(prevDirection, targetDirection, progress / turnDuration), transform.localScale.y, 1);
            ass.Body.transform.localScale = newScale;
            yield return new WaitForEndOfFrame();
        }
        prevDirection = targetDirection;
    }

    private void PlaySound(AudioClip clip, float volume)
    {
        AudioSource audioSource = GameObject.FindGameObjectWithTag("SoundEffectSource").GetComponentInChildren<AudioSource>();
        if (ass.GetComponentInChildren<Renderer>().isVisible)
        {
            if (audioSource != null && clip != null)
            {
                audioSource.volume = volume;
                audioSource.PlayOneShot(clip);
            }
        }
    }

    private bool IsVisibleOnScreen()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        return GeometryUtility.TestPlanesAABB(planes, ass.GetComponentInChildren<Renderer>().bounds);
    }

    private bool IsInBound()
    {
        if(bounds != null)
        {
            return bounds.OverlapPoint(ass.transform.position);
        }
        return true;
    }

}
;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playercontrol : Movecontrol
{
    private TouchControls leftTouch;
    public enum ControlScheme
    {
        KEYBOARD,
        KEYBOARD_MOUSE,
        CONTROLLER,
        TOUCH
    }
    public ControlScheme lastUsedScheme = ControlScheme.KEYBOARD;

    void Start()
    {
        //base.Start();
        //leftTouch = GameObject.Find("VirtualStickLeft").GetComponent<TouchControls>();
        if (Input.GetJoystickNames().Length > 0)
        {
            lastUsedScheme = ControlScheme.CONTROLLER;
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.Android)
        {
            lastUsedScheme = ControlScheme.TOUCH;
        }
        else
        {
            lastUsedScheme = ControlScheme.KEYBOARD;
        }
    }

    // Update is called once per frame
    new void Update()
    {
        if(isDead)
        {
            return;
        }

        horizontal = Input.GetAxis("Horizontal");
        //if (leftTouch.isTouched())
        //{
        //    horizontal = leftTouch.getJoystickPosition().x;
        //    lastUsedScheme = ControlScheme.TOUCH;
        //}
        horizontal = Mathf.Clamp(horizontal, -1, 1);

        if (Input.GetButtonDown("Jump"))
        {
            if (jumpphase == JumpPhase.NOT_JUMPING && groundSensor.isTouching)
            {
                jumpphase = JumpPhase.JUMP_STARTED;
            } else
            {
                if(legs.canWalljump && (leftWallSensor.isTouching || rightWallSensor.isTouching) && !hasWalljumped)
                {
                    jumpphase = JumpPhase.JUMP_STARTED;
                    hasWalljumped = true;
                }
            }
        }
        else if (!Input.GetButton("Jump"))
        {
            if (jumpphase == JumpPhase.JUMPING_UP)
            {
                jumpphase = JumpPhase.FALLING_DOWN;
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            ass.Arms.Use();
        }
        base.Update();
    }
}

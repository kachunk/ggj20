﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerDeactivate : Triggerable
{
    
    private bool triggered = false;
    public override void trigger()
    {
        if (!triggered)
        {
            gameObject.SetActive(false);
            triggered = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

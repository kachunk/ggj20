﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class SingletonUI : MonoBehaviour
{

    public static SingletonUI instance;

    SingletonUI() { }

    public static SingletonUI GetSingletonUI()
    {
        return instance;
    }

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSensor : MonoBehaviour
{
    public bool isTouching = false;
    public bool softTarget = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        softTarget = collision.gameObject.GetComponent<Movecontrol>() != null;
        isTouching = !softTarget;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        softTarget = collision.gameObject.GetComponent<Movecontrol>() != null;
        isTouching = !softTarget;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isTouching = false;
        softTarget = false;
    }
}
